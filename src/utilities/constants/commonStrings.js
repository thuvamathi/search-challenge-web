export const ACCESS_TOKEN = "accessToken";
export const AUTHORIZATION = "Authorization";

export const MESSAGES = {
  LOADING: "Loading...",
  SUCCESS: "Success!",
  UNAUTHORIZED: "Invalid Session / Unauthorized",
  NOT_FOUND: "URI not found!",
  FAILED: "Oops, Something went wrong!",
  INTERNAL_STATUS_ERROR: "(Internal Server Error) Oops, Something went wrong!",
  BAD_REQUEST: "Bad Request!",
  INVALID_CREDENTIALS: "Username or Password is incorrect",

}