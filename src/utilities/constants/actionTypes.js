// Customizer const
export const TOGGLE_COLLAPSED_NAV = "TOGGLE_COLLAPSE_MENU";
export const WINDOW_WIDTH = "WINDOW-WIDTH";
export const SWITCH_LANGUAGE = "SWITCH-LANGUAGE";
export const MENU_CHANGE_SELECTION = "MENU_CHANGE_SELECTION";

export const MAKE_API_REQUEST = "MAKE_API_REQUEST";

// Contact Module const
export const FETCH_START = "fetch_start";
export const FETCH_SUCCESS = "fetch_success";
export const FETCH_ERROR = "fetch_error";
export const SHOW_MESSAGE = "SHOW_MESSAGE";
export const HIDE_MESSAGE = "HIDE_MESSAGE";
export const CLEAR_LOADERS = "CLEAR_LOADERS";
export const ON_SHOW_LOADER = "ON_SHOW_LOADER";
export const ON_HIDE_LOADER = "ON_HIDE_LOADER";

export const NETWORK_STATUS_CODE = {
  SUCCESS: 200,
  UNPROCURABLE_ENTITY: 422,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  EXPECTATION_FAILED: 417,
  INTERNAL_STATUS_ERROR: 500,
  NOT_ACCEPTABLE: 406,
};

// common const
export const COMMON_TYPES = {
  REQUEST: "_REQUEST",
  SUCCESS: "_SUCCESS",
  FAILURE: "_FAILURE",
  RESET: "_RESET",
};

// Auth const
export const AUTH_TYPES = {
  SIGN_IN: "SIGN_IN",
  SIGN_OUT: "SIGN_OUT",
};

// Auth const
export const SEARCH_TYPES = {
  GET_FILTERS: "GET_FILTERS",
  SEARCH: "SEARCH",
};
