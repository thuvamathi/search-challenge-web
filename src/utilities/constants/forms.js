export const FORM_INPUT_TYPES = {
  INPUT_LABEL: "FORM_INPUT_WITH_LABEL",
  DROPDOWN: "FORM_DROPDOWN",
  RADIO: "FORM_RADIO",
  CHECKBOX: "FORM_CHECKBOX",
  TEXTAREA: "FORM_TEXTAREA",
  DATE_RANGE_PICKER: "FORM_DATE_RANGE",
  DATE_PICKER: "FORM_DATE_PICKER",
  TIME_PICKER: "FORM_TIME_PICKER",
  UPLOAD: "FORM_UPLOAD",
  CUSTOM_RENDER: "CUSTOM_RENDER",
  FREQUENCY_SELECTION: "FREQUENCY_SELECTION",
  FORM_PRODUCT_TARGET: "FORM_PRODUCT_TARGET",
};

export const FORM_INPUT_FIELD_TYPES = {
  BUTTONS: "button",
  CHECKBOX: "checkbox",
  COLOR: "color",
  DATE: "date",
  DATE_TIME_LOCAL: "datetime-local",
  EMAIL: "email",
  FILE: "file",
  HIDDEN: "hidden",
  IMAGE: "image",
  MONTH: "month",
  NUMBER: "number",
  ALPHA_NUMERIC: "alphanumeric",
  PASSWORD: "password",
  RADIO: "radio",
  RANGE: "range",
  RESET: "reset",
  SEARCH: "search",
  SUBMIT: "submit",
  TEL: "tel",
  TEXT: "text",
  TIME: "time",
  URL: "url",
  WEEK: "week",
};

export const FORM_INPUT_UPLOAD_ACCEPT_TYPES = {
  ANY: "",
  IMAGES_JPEG_PNG: "image/x-png,image/gif,image/jpeg",
  IMAGES_JPEG: "image/jpeg",
  ALL_IMAGES: "image/*",
  ZIP: ".zip",
  XLSX: ".xlsx",
};

export const FORM_ACCEPT_CONTENT_TYPES = {
  FILE: "application/octet-stream",
  EXCEL: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  PDF: "application/pdf",
};

export default {};
