/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import React from "react";
import { FormattedMessage, injectIntl } from "react-intl";

const InjectMassage = (props) => <FormattedMessage {...props} />;
export default injectIntl(InjectMassage, {
  withRef: false,
});
