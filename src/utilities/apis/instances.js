
import { create } from "apisauce";
import { AUTHORIZATION } from "../constants/commonStrings";

const baseUrl = process.env.REACT_APP_BASE_URL;

/** ApiSauce object to maintain a single api configuration object. */
export const authSauce = create({
  baseURL: `${baseUrl}${process.env.REACT_APP_AUTH_BASE}`,
});

export const searchSauce = create({
  baseURL: `${baseUrl}${process.env.REACT_APP_SEARCH_BASE}`,
});



export const setHeadersForAllInstances = (token) => {
  authSauce.setHeader(AUTHORIZATION, `Bearer ${token}`);
  searchSauce.setHeader(AUTHORIZATION, `Bearer ${token}`);
};

export const removeHeadersForAllInstances = () => {
  authSauce.deleteHeader(AUTHORIZATION);
  searchSauce.deleteHeader(AUTHORIZATION);
};
