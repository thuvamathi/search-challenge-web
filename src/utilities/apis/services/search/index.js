
import { searchSauce } from "../../instances";

// eslint-disable-next-line import/prefer-default-export
export const getFiltersAPI = (payload,config) =>
searchSauce.get(`/api/user/filter?${payload}`,null,config);

export const searchDataAPI = (payload) =>
searchSauce.post(`/api/user/search`, payload);


