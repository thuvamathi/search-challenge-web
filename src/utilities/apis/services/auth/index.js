
import { authSauce } from "../../instances";

// eslint-disable-next-line import/prefer-default-export
export const signInUserAPI = (payload) =>
  authSauce.post(`/api/user/login`, payload);

