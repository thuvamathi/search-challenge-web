// // /*
// //  * Single Retailer Application 24.7.2020
// //  * Copyright © 2020 NCell. All rights reserved.
// //  */

// // import React from "react";
// // import * as turf from "@turf/turf";
// // import moment from "moment";
// // import crypto from "crypto";
// // import jwtDecode from "jwt-decode";
// // import * as log from "loglevel";
// // import _ from "lodash";
// // import { saveAs } from "file-saver";

// // import {
// //   LAYOUT_TYPE_FULL,
// //   LAYOUT_TYPE_BOXED,
// //   LAYOUT_TYPE_FRAMED,
// //   NAV_STYLE_DEFAULT_HORIZONTAL,
// //   NAV_STYLE_DARK_HORIZONTAL,
// //   NAV_STYLE_INSIDE_HEADER_HORIZONTAL,
// //   NAV_STYLE_ABOVE_HEADER,
// //   NAV_STYLE_BELOW_HEADER,
// //   NAV_STYLE_FIXED,
// //   NAV_STYLE_DRAWER,
// //   NAV_STYLE_MINI_SIDEBAR,
// //   NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
// //   NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
// //   ADMIN_MENU,
// //   SERVICES_MENU,
// //   CAMPAIGN_BOX_MENU,
// //   RETAILERS_MENU,
// //   REPORTS_MENUS,
// //   MENU_MAPPINGS,
// //   // POINT_TABLE_TYPES,
// // } from "../constants";
// // // import {
// // //   HorizontalDefault,
// // //   HorizontalDark,
// // //   InsideHeader,
// // //   AboveHeader,
// // //   BelowHeader,
// // //   Topbar,
// // //   NoHeaderNotification,
// // // } from "../../components/templates";
// // import { minFloorSchema } from "../../services/yupSchemaService";

// // export const toDataURL = (url) =>
// //   fetch(url, {
// //     mode: "cors",
// //     headers: {
// //       "Access-Control-Allow-Origin": "*",
// //     },
// //   })
// //     .then((response) => response.blob())
// //     .then(
// //       async (blob) =>
// //         new Promise((resolve, reject) => {
// //           const reader = new FileReader();
// //           reader.onloadend = () => resolve(reader.result);
// //           reader.onerror = reject;
// //           reader.readAsDataURL(blob);
// //         })
// //     );

// export function setLayoutType(_layoutType) {
//   if (_layoutType === LAYOUT_TYPE_FULL) {
//     document.body.classList.remove("boxed-layout");
//     document.body.classList.remove("framed-layout");
//     document.body.classList.add("full-layout");
//   } else if (_layoutType === LAYOUT_TYPE_BOXED) {
//     document.body.classList.remove("full-layout");
//     document.body.classList.remove("framed-layout");
//     document.body.classList.add("boxed-layout");
//   } else if (_layoutType === LAYOUT_TYPE_FRAMED) {
//     document.body.classList.remove("boxed-layout");
//     document.body.classList.remove("full-layout");
//     document.body.classList.add("framed-layout");
//   }
// }

// // export const toBase64 = (file) =>
// //   new Promise((resolve, reject) => {
// //     const reader = new FileReader();
// //     reader.readAsDataURL(file);
// //     reader.onload = () => resolve(reader.result);
// //     reader.onerror = (error) => reject(error);
// //   });

// export function setNavStyle(_navStyle) {
//   if (
//     _navStyle === NAV_STYLE_DEFAULT_HORIZONTAL ||
//     _navStyle === NAV_STYLE_DARK_HORIZONTAL ||
//     _navStyle === NAV_STYLE_INSIDE_HEADER_HORIZONTAL ||
//     _navStyle === NAV_STYLE_ABOVE_HEADER ||
//     _navStyle === NAV_STYLE_BELOW_HEADER
//   ) {
//     document.body.classList.add("full-scroll");
//     document.body.classList.add("horizontal-layout");
//   } else {
//     document.body.classList.remove("full-scroll");
//     document.body.classList.remove("horizontal-layout");
//   }
// }

// export function getNavStyles(navStyle) {
//   switch (navStyle) {
//     case NAV_STYLE_DEFAULT_HORIZONTAL:
//       return <HorizontalDefault />;
//     case NAV_STYLE_DARK_HORIZONTAL:
//       return <HorizontalDark />;
//     case NAV_STYLE_INSIDE_HEADER_HORIZONTAL:
//       return <InsideHeader />;
//     case NAV_STYLE_ABOVE_HEADER:
//       return <AboveHeader />;
//     case NAV_STYLE_BELOW_HEADER:
//       return <BelowHeader />;
//     case NAV_STYLE_FIXED:
//       return <Topbar />;
//     case NAV_STYLE_DRAWER:
//       return <Topbar />;
//     case NAV_STYLE_MINI_SIDEBAR:
//       return <Topbar />;
//     case NAV_STYLE_NO_HEADER_MINI_SIDEBAR:
//       return <NoHeaderNotification />;
//     case NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR:
//       return <NoHeaderNotification />;
//     default:
//       return <Topbar />;
//   }
// }

// export function getContainerClass(navStyle) {
//   switch (navStyle) {
//     case NAV_STYLE_DARK_HORIZONTAL:
//       return "gx-container-wrap";
//     case NAV_STYLE_DEFAULT_HORIZONTAL:
//       return "gx-container-wrap";
//     case NAV_STYLE_INSIDE_HEADER_HORIZONTAL:
//       return "gx-container-wrap";
//     case NAV_STYLE_BELOW_HEADER:
//       return "gx-container-wrap";
//     case NAV_STYLE_ABOVE_HEADER:
//       return "gx-container-wrap";
//     default:
//       return "";
//   }
// }

// // export function checkPolygonIntersect(poly1, poly2) {
// //   const polygon1 = {
// //     type: "Feature",
// //     properties: {},
// //     geometry: {
// //       type: "Polygon",
// //       coordinates: [poly1],
// //     },
// //   };

// //   const polygon2 = {
// //     type: "Feature",
// //     properties: {},
// //     geometry: {
// //       type: "Polygon",
// //       coordinates: [poly2],
// //     },
// //   };
// //   const buffered = turf.buffer(polygon2, -2, { units: "miles" });
// //   const intersection = turf.intersect(polygon1, buffered);
// //   if (intersection !== null) {
// //     return true;
// //   }
// //   return false;
// // }
// // /**
// //  * helper to encrypt any value using a specific algo, key and iv
// //  * @param {string} value
// //  */
// export const encrypt = (value) => {
//   if (process.env.NODE_ENV === "test") {
//     return "";
//   }
//   try {
//     const cipher = crypto.createCipheriv(
//       process.env.REACT_APP_AUTH_CRYPTO_ALGO,
//       Buffer.from(process.env.REACT_APP_AUTH_CRYPTO_KEY),
//       process.env.REACT_APP_AUTH_CRYPTO_IV
//     );

//     const encrypted = cipher.update(value);
//     const encryptedValue = Buffer.concat([encrypted, cipher.final()]);

//     return encryptedValue.toString("hex");
//   } catch (err) {
//     log.error(err);
//     return "";
//   }
// };

// // /**
// //  * helper to decrypt any value using a specific algo, key and iv
// //  * @param {string} encryptedData
// //  */
// // export const decrypt = (encryptedData) => {
// //   try {
// //     const encryptedText = Buffer.from(encryptedData, "hex");
// //     const decipher = crypto.createDecipheriv(
// //       process.env.REACT_APP_AUTH_CRYPTO_ALGO,
// //       Buffer.from(process.env.REACT_APP_AUTH_CRYPTO_KEY),
// //       process.env.REACT_APP_AUTH_CRYPTO_IV
// //     );
// //     const decrypted = decipher.update(encryptedText);
// //     const decryptedString = Buffer.concat([
// //       decrypted,
// //       decipher.final(),
// //     ]).toString();
// //     return JSON.parse(decryptedString);
// //   } catch (err) {
// //     log.error(err);
// //     return null;
// //   }
// // };

// // /**
// //  * Uses jwt-decode to decode any give access token and returns the user object within the decoded token
// //  * @param {string} token access token of the user
// //  */
// // export const getDecodedUser = (token) => {
// //   try {
// //     const decoded = jwtDecode(token);
// //     return decoded ? decoded.pld : null;
// //   } catch (err) {
// //     log.error(err);
// //     return null;
// //   }
// // };

// // export const getNoHeaderClass = (navStyle) => {
// //   if (
// //     navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR ||
// //     navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR
// //   ) {
// //     return "gx-no-header-notifications";
// //   }
// //   return "gx-no-header-notifications";
// // };

// // export const formatTitle = (str = "") => {
// //   const separatedString = str.replace(/_/gi, " ");
// //   return separatedString
// //     .split(" ")
// //     .map((strKey) => strKey.charAt(0).toUpperCase() + strKey.slice(1))
// //     .join(" ");
// // };

// // export const formatTableData = (list = []) => {
// //   return list.map((item, index) => {
// //     return { ...item, key: index };
// //   });
// // };

// // export const getCenterOfPolygon = (coordinates) => {
// //   const polygon = turf.polygon([coordinates]);
// //   return turf.centroid(polygon);
// // };

// // export const formatLatLng = (array) => {
// //   const data = array.map((item) => {
// //     return [item.lat, item.lng];
// //   });
// //   return [...data, [array[0].lat, array[0].lng]];
// // };
// // export const removeColumnListItems = (columnsList, keyList) => {
// //   keyList.forEach((key) => {
// //     const index = columnsList.indexOf(key);
// //     if (index > -1) {
// //       columnsList.splice(index, 1);
// //     }
// //   });
// //   columnsList.push("action");
// //   return columnsList;
// // };

// // export const formatFieldData = async (value, type, format) => {
// //   switch (type) {
// //     case "string":
// //       return `${value}`;
// //     case "number":
// //       return Number.parseInt(`${value}`, 10);
// //     case "boolean":
// //       return (
// //         Number.isInteger(Number.parseInt(`${value}`, 10)) &&
// //         Number.parseInt(`${value}`, 10) === 1
// //       );
// //     case "date":
// //       return moment(value).format(format);
// //     case "dates": {
// //       const dates = value.map((date) => moment(date).format(format));
// //       return dates;
// //     }
// //     case "status":
// //       return `${value}`.toUpperCase();
// //     case "file": {
// //       if (!value) {
// //         return "";
// //       }
// //       if (Array.isArray(value)) {
// //         const baseDataFromUrl = await toDataURL(value[0].url);
// //         return baseDataFromUrl;
// //       }
// //       if (value instanceof String) {
// //         return value;
// //       }
// //       if (value.file) {
// //         const base64 = await toBase64(value.file.originFileObj || value.file);
// //         return base64;
// //       }
// //       return "";
// //     }
// //     default:
// //       return value;
// //   }
// // };

// // export const formatFormFields = (formData, schema) => {
// //   const keys = Object.keys(formData);
// //   const formattedData = {};

// //   const keySize = keys.length;
// //   let keyCount = 0;

// //   return new Promise((resolve) => {
// //     keys.forEach(async (key) => {
// //       const formItem = formData[key];
// //       const rule = schema[key] || {};
// //       const ruleFormat = rule.format || "DD-MM-YYYY";

// //       formattedData[key] = await formatFieldData(
// //         formItem,
// //         rule.type,
// //         ruleFormat
// //       );

// //       keyCount += 1;

// //       if (keyCount === keySize) {
// //         resolve(formattedData);
// //       }
// //     });
// //     // log.info("formattedData", formattedData);
// //   });
// // };

// // export const disableDateBeforeToday = (current) => {
// //   return current && current < moment().subtract(1, "days");
// // };

// // export const disabledMinutesList = (hour, date) => {
// //   const disableMinutes = [];
// //   if (moment(date).format("YYYY/MM/DD") > moment().format("YYYY/MM/DD")) {
// //     return disableMinutes;
// //   }
// //   if (moment(date).format("DD/MM/YYYY") === moment().format("DD/MM/YYYY")) {
// //     if (hour === moment().hour()) {
// //       for (let i = 0; i <= moment().minute(); i += 1) {
// //         disableMinutes.push(i);
// //       }
// //       return disableMinutes;
// //     }
// //     return disableMinutes;
// //   }
// //   for (let i = 0; i <= 60; i += 1) {
// //     disableMinutes.push(i);
// //   }
// //   return disableMinutes;
// // };

// // export const formatDate = (date, format, displayFormat = "DD/MM/YYYY") => {
// //   return moment(date, format).format(displayFormat);
// // };

// // export const formatTime = (time, format, displayFormat = "HH:mm") => {
// //   return moment(time, format).format(displayFormat);
// // };

// // export const combineDateAndTime = (date, time) => {
// //   return `${date} at ${time}`;
// // };

// // export const getDateFromSchedule = (dateAndTime) => {
// //   const dateTimeArray = dateAndTime.split("T");
// //   return dateTimeArray[0];
// // };

// // export const getTimeFromSchedule = (dateAndTime) => {
// //   const dateTimeArray = dateAndTime.split("T");
// //   return dateTimeArray[1];
// // };

// // export const formatBoolStatusToText = (status = 0) => {
// //   return status === 1 ? "True" : "False";
// // };

// // export const formatYesNoStatusToText = (status = 0) => {
// //   return status === 1 ? "Yes" : "No";
// // };

// // export const formatLOB = (lob) => {
// //   switch (lob) {
// //     case "prepaid":
// //       return "Prepaid";
// //     case "postpaid":
// //       return "Postpaid";
// //     default:
// //       return "";
// //   }
// // };

// // export const formatProductCategory = (category) => {
// //   switch (category) {
// //     case "voice":
// //       return "Voice";
// //     case "data":
// //       return "Data";
// //     case "sms":
// //       return "SMS";
// //     default:
// //       return "N/A";
// //   }
// // };

// // export const formatVASCategory = (category) => {
// //   switch (category) {
// //     case "prepaid":
// //       return "Prepaid";
// //     case "postpaid":
// //       return "Postpaid";
// //     default:
// //       return "All";
// //   }
// // };

// // export const formatStatusText = (status) => {
// //   switch (status) {
// //     case "ACTIVE":
// //       return "active";
// //     case "INACTIVE":
// //       return "inactive";
// //     default:
// //       return "inactive";
// //   }
// // };

// // export const formatNotificationStatusText = (status) => {
// //   switch (status) {
// //     case "SCHEDULED":
// //       return "scheduled";
// //     case "ONHOLD":
// //       return "onHold";
// //     case "EXECUTED":
// //       return "executed";
// //     default:
// //       return "scheduled";
// //   }
// // };

// // export const formatStatusNumber = (status) => {
// //   switch (status) {
// //     case 1:
// //       return "active";
// //     case 2:
// //       return "inactive";
// //     default:
// //       return "inactive";
// //   }
// // };

// // export const getBannerImageName = (url) => {
// //   if (url) {
// //     const splitUrls = url.split("/");

// //     return splitUrls[splitUrls.length - 1];
// //   }

// //   return "";
// // };

// // export const formatArrayIndex = (index, prefix) => {
// //   if (prefix) {
// //     return `${prefix}-${index}`;
// //   }
// //   return index;
// // };

// // export const validateEmail = (email) => {
// //   const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
// //   return re.test(String(email).toLowerCase());
// // };

// // export const cleanObjects = (obj) => {
// //   const keys = Object.keys(obj);
// //   const _obj = obj;

// //   keys.forEach((key) => {
// //     if (obj[key] === null || obj[key] === undefined) {
// //       delete _obj[key];
// //     }
// //   });

// //   return _obj;
// // };

// // export const getChangedField = (changedFields) => {
// //   if (
// //     changedFields &&
// //     Array.isArray(changedFields) &&
// //     changedFields.length > 0
// //   ) {
// //     if (changedFields[0].validating) {
// //       return false;
// //     }
// //     return {
// //       fieldName: changedFields[0].name[0],
// //       fieldValue: changedFields[0].value,
// //     };
// //   }
// //   return {};
// // };

// // export const formatBannerURLIDField = (type) => {
// //   switch (type) {
// //     case "DEFAULT":
// //       return "Default";
// //     case "PRODUCT":
// //       return "Product Name";
// //     case "CAMPAIGN_BOX":
// //       return "Campaign Name";
// //     default:
// //       return "Banner URL";
// //   }
// // };

// // export const formatBannerURLTypeValue = (type) => {
// //   switch (type) {
// //     case "DEFAULT":
// //       return "Default";
// //     case "PRODUCT":
// //       return "Product";
// //     case "CAMPAIGN_BOX":
// //       return "Campaign";
// //     default:
// //       return "Banner URL";
// //   }
// // };

// // export const formatNotificationType = (type) => {
// //   switch (type) {
// //     case "all":
// //       return "All";
// //     case "master":
// //       return "Master";
// //     case "vft":
// //       return "VFT";
// //     default:
// //       return "All";
// //   }
// // };

// // export const disabledDaysBeforeToday = (current) => {
// //   // Can not select days before today and today
// //   return current && current < moment().startOf("day");
// // };

// // export const disabledDaysBeforeDate = (current, date) => {
// //   // Can not select days before today and today
// //   return current && current > moment(date).startOf("day");
// // };

// // export const disabledDaysAfterToday = (current) => {
// //   return current && current > moment(moment().add(1, "days")).startOf("day");
// // };

// // export const disabledDaysAfterDate = (current, date) => {
// //   // Can not select days before today and today
// //   return current && current > moment(date).startOf("day");
// // };

// // export const disabledDaysBeforeThreeMonths = (current) => {
// //   return (
// //     current && current < moment(moment().subtract(3, "months")).startOf("day")
// //   );
// // };

// // export const getDisabledHours = (date) => {
// //   const hours = [];

// //   if (moment(date).format("YYYY/MM/DD") > moment().format("YYYY/MM/DD")) {
// //     return hours;
// //   }
// //   if (
// //     date &&
// //     moment(date).format("YYYY/MM/DD") === moment().format("YYYY/MM/DD")
// //   ) {
// //     for (let i = 0; i < moment().hour(); i += 1) {
// //       hours.push(i);
// //     }
// //     return hours;
// //   }
// //   for (let i = 0; i < 24; i += 1) {
// //     hours.push(i);
// //   }
// //   return hours;
// // };

// // export const getDisabledMinutes = (selectedHour) => {
// //   const minutes = [];
// //   if (selectedHour === moment().hour()) {
// //     for (let i = 0; i < moment().minute(); i += 1) {
// //       minutes.push(i);
// //     }
// //   }
// //   return minutes;
// // };

// // export const base64toBlob = (b64Data, contentType = "", sliceSize = 512) => {
// //   const byteCharacters = atob(b64Data);
// //   const byteArrays = [];

// //   for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
// //     const slice = byteCharacters.slice(offset, offset + sliceSize);

// //     const byteNumbers = new Array(slice.length);
// //     for (let i = 0; i < slice.length; i += 1) {
// //       byteNumbers[i] = slice.charCodeAt(i);
// //     }

// //     const byteArray = new Uint8Array(byteNumbers);
// //     byteArrays.push(byteArray);
// //   }

// //   const blob = new Blob(byteArrays, { type: contentType });
// //   return blob;
// // };

// // export const downloadFileFromBase64 = (
// //   b64Data,
// //   contentType,
// //   name = "bulk.xlsx"
// // ) => {
// //   const blob = base64toBlob(b64Data, contentType);
// //   // const blobUrl = URL.createObjectURL(blob);

// //   saveAs(blob, name);
// // };

// // export const downloadPdfFromBase64 = (b64Data, contentType) => {
// //   const blob = base64toBlob(b64Data, contentType);
// //   saveAs(blob, "simChangeForm.pdf");
// // };

// // export const validateAlphaNumeric = (value) => {
// //   const regex = /^[a-z0-9]+$/i;

// //   return regex.test(value);
// // };

// // export const setIdFieldMandotary = (
// //   value,
// //   setIsIdMandotary,
// //   setIsMsisdnRequired
// // ) => {
// //   if (value) {
// //     setIsIdMandotary(true);
// //     setIsMsisdnRequired(false);
// //   } else {
// //     setIsMsisdnRequired(true);
// //   }
// // };

// // export const getSubscriptionDuration = (
// //   startDate,
// //   subscriptionDuration,
// //   campaignDuration
// // ) => {
// //   const splitCampaign = campaignDuration.split(" ");
// //   const splitSubscription = subscriptionDuration.split(" ");
// //   if (subscriptionDuration === "N/A") {
// //     return `${moment(startDate).format(moment.HTML5_FMT.DATE)} - ${moment(
// //       startDate
// //     )
// //       .add(splitCampaign[0], splitCampaign[1])
// //       .format(moment.HTML5_FMT.DATE)}`;
// //   }
// //   return `${moment(startDate)
// //     .subtract(splitSubscription[0], splitSubscription[1])
// //     .format(moment.HTML5_FMT.DATE)} - ${moment(startDate)
// //     .add(splitCampaign[0], splitCampaign[1])
// //     .format(moment.HTML5_FMT.DATE)}`;
// // };

// // export const getCampaignType = (type, optionsList = []) => {
// //   let campaignType = null;
// //   optionsList.forEach((item) => {
// //     if (item.id === type) {
// //       campaignType = item.campaigntype.name;
// //     }
// //   });
// //   return campaignType;
// // };

// // const filterMenuWithPermissions = (menuList = {}, menu, permissions) => {
// //   const _menuList = menuList;
// //   if (menu.actions.length > 0) {
// //     const filteredMenus = menu.actions.filter((actionMenu) => {
// //       if (actionMenu.actions && actionMenu.actions.length > 0) {
// //         const subMenus = actionMenu.actions.filter((subMenuAction) => {
// //           const found = permissions.some(
// //             (r) => subMenuAction.actionPermissions.indexOf(r) >= 0
// //           );
// //           return found;
// //         });

// //         // eslint-disable-next-line no-param-reassign
// //         actionMenu.actions = subMenus;

// //         return subMenus.length > 0;
// //       }
// //       if (
// //         actionMenu.actionPermissions &&
// //         actionMenu.actionPermissions.length > 0
// //       ) {
// //         const found = permissions.some(
// //           (r) => actionMenu.actionPermissions.indexOf(r) >= 0
// //         );
// //         return found;
// //       }

// //       return false;
// //     });

// //     if (filteredMenus.length > 0) {
// //       _menuList[menu.groupId] = {
// //         ...menu,
// //         actions: filteredMenus,
// //       };
// //     }
// //   } else if (menu.actionPermissions && menu.actionPermissions.length > 0) {
// //     const found = permissions.some(
// //       (r) => menu.actionPermissions.indexOf(r) >= 0
// //     );

// //     if (found) {
// //       _menuList[menu.groupId] = {
// //         ...menu,
// //       };
// //     }
// //   }

// //   return _menuList;
// // };

// // export const decryptMenu = (userData) => {
// //   const { profile } = userData || {};
// //   // eslint-disable-next-line camelcase
// //   const { profile_permission } = profile || {};

// //   const permissions = profile_permission.map(
// //     (permission) => permission.action_name
// //   );
// //   let menus = {};

// //   menus = filterMenuWithPermissions(menus, ADMIN_MENU, permissions);
// //   menus = filterMenuWithPermissions(menus, SERVICES_MENU, permissions);
// //   menus = filterMenuWithPermissions(menus, CAMPAIGN_BOX_MENU, permissions);
// //   menus = filterMenuWithPermissions(menus, RETAILERS_MENU, permissions);
// //   menus = filterMenuWithPermissions(menus, REPORTS_MENUS, permissions);

// //   const menuList = Object.values(menus);
// //   const initialPage =
// //     menuList.length > 0
// //       ? (
// //           Object.values(MENU_MAPPINGS).filter((menuMap) => {
// //             if (menuList[0].actions && menuList[0].actions.length > 0) {
// //               if (
// //                 menuList[0].actions &&
// //                 menuList[0].actions.length > 0 &&
// //                 menuList[0].actions[0].actions.length > 0
// //               ) {
// //                 return (
// //                   menuMap.id === menuList[0].actions[0].actions[0].pathMapId
// //                 );
// //               }
// //               return menuMap.id === menuList[0].actions[0].pathMapId;
// //             }
// //             return menuMap.id === menuList[0].pathMapId;
// //           })[0] || {}
// //         ).path
// //       : MENU_MAPPINGS.DEFAULT.path;

// //   return {
// //     initialPage: initialPage || MENU_MAPPINGS.DEFAULT.path,
// //     menus: menuList,
// //     permissions,
// //   };
// // };

// // export const validatePermissions = (permission, permissions = []) => {
// //   const found = permissions.some((r) => [permission].indexOf(r) >= 0);
// //   return found;
// // };

// export const validatePathWithPermissions = (menuList, permissions) => {
//   if (!menuList) {
//     return {};
//   }
//   const _menuList = {};
//   Object.keys(menuList).forEach((key) => {
//     if (validatePermissions(menuList[key].permittedTo, permissions)) {
//       _menuList[key] = menuList[key];
//     }
//   });
//   return _menuList;
// };

// // export const campaignSelector = (state) => state.campaign;
// export const authSelector = (state) => state.auth;

// // export const generateCreateCampaignPayload = (
// //   campaignState,
// //   selectedCampaign
// // ) => ({
// //   campaignId: campaignState.createCampaign.campaignId,
// //   campaignName: campaignState.createCampaign.campaignName,
// //   campaignDescription: campaignState.createCampaign.campaignDesc,
// //   startDate: moment(campaignState.createCampaign.startDate).format(
// //     moment.HTML5_FMT.DATE
// //   ),
// //   campaignType: campaignState.createCampaign.campaignType,
// //   averageSalesHistoryPeriod: campaignState.createCampaign.avgSalesHistoryPeriod,
// //   subscriptionDuration: campaignState.createCampaign.subscriptionDuration,
// //   campaignDuration: campaignState.createCampaign.campaignDuration,
// //   retailerType: campaignState.kpiConfigurations.retailerType,
// //   municipals: campaignState.kpiConfigurations.municipal,
// //   provinces: campaignState.kpiConfigurations.province,
// //   districts: campaignState.kpiConfigurations.district,
// //   kpi: campaignState.kpiConfigurations.kpiItems.map((kpi) => ({
// //     kpi: kpi.id,
// //     maxCeiling: kpi.alternateCeil ? kpi.maxCeil : null,
// //     mandatoryKpi: kpi.mandatoryKpi ? 1 : 0,
// //     achievementMandatory: kpi.achievementMandatory ? 1 : 0,
// //     minimumFloorOverrides: kpi.minFloor
// //       .filter((minFloorItem) => {
// //         try {
// //           minFloorSchema.validateSync(minFloorItem);
// //           return true;
// //         } catch (err) {
// //           return false;
// //         }
// //       })
// //       .map((minFloorItem) => ({
// //         district: minFloorItem.district,
// //         minValue: minFloorItem.minValue,
// //       })),

// //     slabMatrixes: kpi.slabs.map((slabItem) => ({
// //       bauMin: slabItem.bauMin,
// //       bauMax: slabItem.bauMax,
// //       floorValue: slabItem.floorVal,
// //       ceilingValue: slabItem.ceilVal,
// //     })),
// //     pointTableConfigurations:
// //       selectedCampaign && selectedCampaign.code === "PB"
// //         ? [
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.additionUnit || 0,
// //               pointType: POINT_TABLE_TYPES.POINTS_ADDITION,
// //               pointPerUnit: innerItem.additionPointsPerUnit || 0,
// //             })),
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.deductionUnit || 0,
// //               pointType: POINT_TABLE_TYPES.POINTS_DEDUCTION,
// //               pointPerUnit: innerItem.deductionPointsPerUnit || 0,
// //             })),
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.bonusUnit || 0,
// //               pointType: POINT_TABLE_TYPES.BONUS_POINTS,
// //               pointPerUnit: innerItem.bonusPointsPerUnit || 0,
// //             })),
// //           ]
// //         : _.flatten(
// //             campaignState.flatPointTable
// //               .filter((innerItem) => innerItem.key === kpi.id)
// //               .map((innerItem) => [
// //                 {
// //                   pointUnit: innerItem.additionUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.POINTS_ADDITION,
// //                   pointPerUnit: innerItem.additionPointsPerUnit || 0,
// //                 },
// //                 {
// //                   pointUnit: innerItem.deductionUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.POINTS_DEDUCTION,
// //                   pointPerUnit: innerItem.deductionPointsPerUnit,
// //                 },
// //                 {
// //                   pointUnit: innerItem.bonusUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.BONUS_POINTS,
// //                   pointPerUnit: innerItem.bonusPointsPerUnit || 0,
// //                 },
// //               ])
// //           ),
// //   })),
// //   giftTable: {
// //     startingPointValue: Number(
// //       campaignState.giftTableConfigurations.startPoint
// //     ),
// //     highestPointValue: Number(
// //       campaignState.giftTableConfigurations.highestPoint
// //     ),
// //     interval: Number(campaignState.giftTableConfigurations.interval),
// //     giftPurchaseAmount: Number(
// //       campaignState.giftTableConfigurations.giftPurchaseAmount
// //     ),
// //     cashPurchaseAmount: Number(
// //       campaignState.giftTableConfigurations.cashPurchaseAmount
// //     ),
// //     configurations: campaignState.giftTableSlabs.map((giftSlab) => ({
// //       lowerLimit: giftSlab.lowerLimit,
// //       upperLimit: giftSlab.higherLimit,
// //       cashAmount: giftSlab.cashAmount,
// //       giftAmount: giftSlab.giftAmount,
// //       giftSelectionOneID: giftSlab.gift1,
// //       giftSelectionTwoID: giftSlab.gift2 || null,
// //       giftSelectionThreeID: giftSlab.gift3 || null,
// //     })),
// //   },
// // });

// // export const generateUpdateCampaignPayload = (campaignState, action) => ({
// //   campaignId: Number(action.payload.id),
// //   kpi: campaignState.kpiConfigurations.kpiItems.map((kpi) => ({
// //     kpi: kpi.id,
// //     maxCeiling: kpi.alternateCeil ? kpi.maxCeil : null,
// //     mandatoryKpi: kpi.mandatoryKpi ? 1 : 0,
// //     achievementMandatory: kpi.achievementMandatory ? 1 : 0,
// //     minimumFloorOverrides: kpi.minFloor
// //       .filter((minFloorItem) => {
// //         try {
// //           minFloorSchema.validateSync(minFloorItem);
// //           return true;
// //         } catch (err) {
// //           return false;
// //         }
// //       })
// //       .map((minFloorItem) => ({
// //         district: minFloorItem.district,
// //         minValue: minFloorItem.minValue,
// //       })),

// //     slabMatrixes: kpi.slabs.map((slabItem) => ({
// //       bauMin: slabItem.bauMin,
// //       bauMax: slabItem.bauMax,
// //       floorValue: slabItem.floorVal,
// //       ceilingValue: slabItem.ceilVal,
// //     })),
// //     pointTableConfigurations:
// //       action.payload.campaigntype.code === "PB"
// //         ? [
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.additionUnit || 0,
// //               pointType: POINT_TABLE_TYPES.POINTS_ADDITION,
// //               pointPerUnit: innerItem.additionPointsPerUnit || 0,
// //             })),
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.deductionUnit || 0,
// //               pointType: POINT_TABLE_TYPES.POINTS_DEDUCTION,
// //               pointPerUnit: innerItem.deductionPointsPerUnit || 0,
// //             })),
// //             ...kpi.slabs.map((innerItem) => ({
// //               bauMin: innerItem.bauMin || 0,
// //               bauMax: innerItem.bauMax || 0,
// //               pointUnit: innerItem.bonusUnit || 0,
// //               pointType: POINT_TABLE_TYPES.BONUS_POINTS,
// //               pointPerUnit: innerItem.bonusPointsPerUnit || 0,
// //             })),
// //           ]
// //         : _.flatten(
// //             campaignState.flatPointTable
// //               .filter((innerItem) => innerItem.key === kpi.id)
// //               .map((innerItem) => [
// //                 {
// //                   pointUnit: innerItem.additionUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.POINTS_ADDITION,
// //                   pointPerUnit: innerItem.additionPointsPerUnit || 0,
// //                 },
// //                 {
// //                   pointUnit: innerItem.deductionUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.POINTS_DEDUCTION,
// //                   pointPerUnit: innerItem.deductionPointsPerUnit,
// //                 },
// //                 {
// //                   pointUnit: innerItem.bonusUnit || 0,
// //                   pointType: POINT_TABLE_TYPES.BONUS_POINTS,
// //                   pointPerUnit: innerItem.bonusPointsPerUnit || 0,
// //                 },
// //               ])
// //           ),
// //   })),
// // });

// export default { setLayoutType };
