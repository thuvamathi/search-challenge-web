/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import React, { Component } from "react";
import Nprogress from "nprogress";

import "nprogress/nprogress.css";
import { CircularProgress } from "../components/atoms";

export default function asyncComponent(importComponent) {
  class AsyncFunc extends Component {
    constructor(props) {
      super(props);
      this.state = {
        component: null,
      };
    }

    // eslint-disable-next-line react/no-deprecated
    componentWillMount() {
      Nprogress.start();
    }

    async componentDidMount() {
      this.mounted = true;
      const Comp = importComponent;
      Nprogress.done();
      if (this.mounted) {
        this.setState({
          component: <Comp {...this.props} />,
        });
      }
    }

    componentWillUnmount() {
      this.mounted = false;
    }

    render() {
      const { component } = this.state;
      const _Component = component || <CircularProgress />;
      return (
        <div type="text" rows={7} ready={`${_Component !== null}`}>
          {_Component}
        </div>
      );
    }
  }

  return AsyncFunc;
}
