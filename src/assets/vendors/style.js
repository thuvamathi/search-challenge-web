/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import "./flag/sprite-flags-24x24.css";
import "./gaxon/styles.css";
import "react-big-calendar/lib/css/react-big-calendar.css";
import "./noir-pro/styles.css";
