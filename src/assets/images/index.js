
import logo from "./logo/logo.png";
import profileAvatar from "./profileAvatar.jpg";

export default {
  logo,
  profileAvatar,
};
