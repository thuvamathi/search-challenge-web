import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import URLSearchParams from "url-search-params";
import { Route, Switch } from "react-router-dom";
import { ConfigProvider } from "antd";
import { IntlProvider } from "react-intl";
import PropTypes from "prop-types";

import PrivateRoute from "../routes/privateRoute";
import PublicRoute from "../routes/publicRoute";

import MainApp from "./app.component";
import Login from "../components/containers/loginPage";
import PageNotFound from "../components/templates/404Template";
import AppLocale from "../assets/locale";
import {  PATHS } from "../utilities/constants";


const App = (props) => {
  const { location } = props;

  const dispatch = useDispatch();

  const locale = useSelector(({ settings }) => settings.locale);

  useEffect(() => {
    const params = new URLSearchParams(location.search);
  }, [dispatch, location.search]);

  const currentAppLocale = AppLocale[locale.locale];

  return (
    <ConfigProvider locale={currentAppLocale.antd}>
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}
      >
        <Switch>
          <PublicRoute path={PATHS.LOGIN} Component={Login} exact />
          <PrivateRoute
            path={PATHS.DASHBOARD.WILD_CARD}
            Component={MainApp}
            exact
          />
          <Route path={PATHS.WILD_CARD} component={PageNotFound} />
        </Switch>
      </IntlProvider>
    </ConfigProvider>
  );
};

App.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
    pathname: PropTypes.string,
  }).isRequired,
  match: PropTypes.shape({
    url: PropTypes.string,
  }).isRequired,
};

export default App;
