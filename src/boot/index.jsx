import React from "react";
import ReactDOM from "react-dom";
import * as log from "loglevel";

import { AppContainer } from "react-hot-loader";

import NextApp from "./nextApp";
import registerServiceWorker from "./registerServiceWorker";

log.setLevel("INFO");

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById("root")
  );
};

registerServiceWorker();
render(NextApp);

if (module.hot) {
  module.hot.accept("./nextApp", () => {
    render(NextApp);
  });
}
