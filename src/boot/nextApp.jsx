import React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { ConnectedRouter } from "connected-react-router";
import { Route, Switch } from "react-router-dom";

import "../assets/vendors/style";
import "../assets/theme/sra-theme.less";

import configureStore, { history } from "../redux/store";
import App from "./app.index";

const { store, persistor } = configureStore(/ provide initial state if any /);

const NextApp = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/" component={App} />
        </Switch>
      </ConnectedRouter>
    </PersistGate>
  </Provider>
);

export default NextApp;
