
import React from "react";
import { Layout } from "antd";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

import App from "../routes";


const { Content } = Layout;

const MainApp = (props) => {
  const navStyle = useSelector(({ settings }) => settings.navStyle);
  const width = useSelector(({ settings }) => settings.width);


  const { match } = props;

  return (
    <Layout className="gx-app-layout" style = {{background:"white"}}>
      <Layout>
        <Content className={`gx-layout-content`}>
          <App match={match} />
        </Content>
      </Layout>
    </Layout>
  );
};

MainApp.propTypes = {
  match: PropTypes.shape.isRequired,
};

export default MainApp;
