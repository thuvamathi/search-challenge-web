import React from "react";
import PropTypes from "prop-types";
import {
    Form,
    Input,
    Button,
    Select
} from 'antd';

const SearchForm = ({ onSubmit,getFilters,typeList}) => {
    const [form] = Form.useForm();
    const layout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 10 },
    };

    const tailLayout = {
        wrapperCol: { offset: 12, span: 10 },
    };

    return(
        <Form
                style = {{margin:"30px"}}
                form={form}
                onFinish={onSubmit}
                {...layout}
            >
                <Form.Item label="Type" name="type" rules={[{ required: true, message: 'Please select type' }]}>
                    <Select onChange={getFilters}>
                        <Select.Option value="organization">Organization</Select.Option>
                        <Select.Option value="user">Users</Select.Option>
                        <Select.Option value="ticket">Tickets</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item label="Column" name="column" rules={[{ required: true, message: 'Please select column' }]}>
                    {
                        Object.keys(typeList).length === 0 && typeList.constructor === Object ? <Select disabled></Select> :
                            <Select >
                                {
                                    Object.keys(typeList).map((item, value) => {
                                        return (
                                            <Select.Option key={value} value={item}>{typeList[item]}</Select.Option>
                                        )
                                    })
                                }
                            </Select>
                    }

                </Form.Item>

                <Form.Item label="Value" name="value">
                    <Input />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button type="primary" onClick={form.submit}>Search</Button>
                </Form.Item>
            </Form>
    )
}

SearchForm.propTypes = {
    onSubmit: PropTypes.func,
    getFilters: PropTypes.func,
    typeList:PropTypes.object
};

SearchForm.defaultProps = {
    onSubmit:()=>{},
    getFilters: ()=>{},
    typeList:{}
};

export default SearchForm;
