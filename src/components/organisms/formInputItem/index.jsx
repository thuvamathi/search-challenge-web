import React, { useState } from "react";
import PropTypes from "prop-types";
import { Input, Form } from "antd";

import { FORM_INPUT_FIELD_TYPES } from "../../../utilities/constants";
import "./styles.less";
// import { validateAlphaNumeric } from "../../../utilities/helpers";

const FormItem = Form.Item;

const FormInputItem = (props) => {
  const {
    name,
    label,
    inputId,
    placeholder,
    inputPrefixComp,
    addonAfter,
    showCharCounter,
    charLimit,
    inputType,
    readOnly,
    defaultValue,
    fieldKey,
    validationRules,
    inputValue,
    handleChange,
    onCountReset,
    onBlur,
  } = props;

  const [stateVal, setValue] = useState(`${defaultValue}` || "");

  if (onCountReset) {
    onCountReset(() => {
      setValue(defaultValue || "");
      handleChange(defaultValue || "");
    });
  }

  return (
    <FormItem className={`form-input-${addonAfter ? "masked-" : ""}container`}>
      <FormItem
        key={fieldKey}
        name={name}
        className={`form-input-${addonAfter ? "masked-" : ""}item`}
        label={label}
        rules={validationRules}
        initialValue={inputValue || defaultValue}
      >
        <Input
          placeholder={placeholder}
          id={inputId}
          prefix={inputPrefixComp ? inputPrefixComp() : null}
          addonAfter={addonAfter}
          type={
            inputType &&
            (inputType === FORM_INPUT_FIELD_TYPES.NUMBER ||
              inputType === FORM_INPUT_FIELD_TYPES.ALPHA_NUMERIC)
              ? "text"
              : inputType
          }
          readOnly={readOnly}
          maxLength={charLimit}
          defaultValue={defaultValue}
          min={0}
          onBlur={onBlur}
          onKeyDown={(e) => {
            if (
              inputType === FORM_INPUT_FIELD_TYPES.NUMBER &&
              e.key !== "Backspace" &&
              e.key !== "." &&
              Number.isNaN(Number.parseFloat(`${e.key}`, 10))
            ) {
              e.preventDefault();
              return false;
            }

            if (
              inputType === FORM_INPUT_FIELD_TYPES.ALPHA_NUMERIC &&
              e.key !== "Backspace"
              
            ) {
              e.preventDefault();
              return false;
            }

            return true;
          }}
          onChange={(event) => {
            const { value } = event.target || {};
            let editedValue = value || "";

            if (addonAfter) {
              editedValue = editedValue
                .replace(addonAfter, "")
                .substring(0, charLimit);
            } else if (editedValue.length > charLimit) {
              editedValue = editedValue.substring(0, charLimit);
            }

            setValue(editedValue);
            handleChange(editedValue);
          }}
          value={inputValue}
        />
      </FormItem>
      {showCharCounter && (
        <div className="input-item-char-limit">
          {`${inputValue ? inputValue.length : stateVal.length}/${charLimit}`}
        </div>
      )}
    </FormItem>
  );
};

FormInputItem.propTypes = {
  fieldKey: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  addonAfter: PropTypes.string,
  placeholder: PropTypes.string,
  inputId: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  showCharCounter: PropTypes.bool,
  inputPrefixComp: PropTypes.func,
  charLimit: PropTypes.number,
  inputType: PropTypes.string,
  defaultValue: PropTypes.string,
  validationRules: PropTypes.arrayOf(PropTypes.shape()),
  inputValue: PropTypes.string,
  handleChange: PropTypes.func,
  onCountReset: PropTypes.func,
  onBlur: PropTypes.func,
};

FormInputItem.defaultProps = {
  placeholder: "",
  addonAfter: null,
  readOnly: false,
  showCharCounter: false,
  inputPrefixComp: () => <div />,
  charLimit: 100,
  inputType: FORM_INPUT_FIELD_TYPES.TEXT,
  defaultValue: "",
  validationRules: [],
  inputValue: null,
  handleChange: () => {},
  onCountReset: () => {},
  onBlur: () => {},
};

export default FormInputItem;
