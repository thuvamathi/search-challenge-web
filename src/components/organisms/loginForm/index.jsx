/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import React, { useState } from "react";
import PropTypes from "prop-types";
import "./styles.less";
import { Input, Form, Checkbox } from "antd";
import { useIntl } from "react-intl";
import { PrimeButton } from "../../atoms";

import logo from "../../../assets/images/logo/logo.png";
import loginPasswordImg from "../../../assets/images/login-password.png";
import loginEmailImg from "../../../assets/images/login-username.png";

const LoginForm = ({ login }) => {
  const intl = useIntl();
  const loginButtonText = intl.formatMessage({
    id: "login.button",
  });
  const loginRememberMe = intl.formatMessage({
    id: "login.remember",
  });
  const loginEmail = intl.formatMessage({
    id: "login.username",
  });
  const loginPasswordText = intl.formatMessage({
    id: "login.password",
  });
  const loginEmailError = intl.formatMessage({
    id: "login.username.error",
  });
  const loginPasswordError = intl.formatMessage({
    id: "login.password.error",
  });
  const [form] = Form.useForm();
  return (
    <div className="login-form-wrap">
      <div className="logo-wrap">
        <img className="logo-img" alt="logo-img" src={logo} />
      </div>
      <div className="input-wrap">
        <Form
          form={form}
          className="input-wrap-form"
          name="login"
          initialValues={{
            username: "",
            password: "",
            remember: false,
          }}
          autoComplete="off"
        >
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: loginEmailError,
              },
              () => ({
                validator(rule, value) {
                  const pattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
                  if (!pattern.test(value)) {
                    return Promise.reject(Error(loginEmailError));
                  }

                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input
              placeholder={loginEmail}
              className="login-input"
              autoComplete="off"
              type="text"
              prefix={
                <img
                  className="input-icon"
                  alt="img-email"
                  src={loginEmailImg}
                />
              }
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: loginPasswordError }]}
          >
            <Input.Password
              placeholder={loginPasswordText}
              autoComplete="off"
              type="text"
              prefix={
                <img
                  className="input-icon"
                  alt="img-password"
                  src={loginPasswordImg}
                />
              }
            />
          </Form.Item>
          <Form.Item
            name="remember"
            valuePropName="checked"
            className="remember-wrap"
          >
            <Checkbox className="remember">{loginRememberMe}</Checkbox>

          </Form.Item>
          <PrimeButton
              buttonName={loginButtonText}
              onClick={async () => {
                try {
                  const fields = await form.validateFields();
                  login(fields);
                } catch (err) {
                  // do nothing
                }
              }}
            />
        </Form>
        
      </div>
    </div>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
};

export default LoginForm;
