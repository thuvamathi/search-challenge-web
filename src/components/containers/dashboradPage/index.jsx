import React from "react";
import { useDispatch } from "react-redux";
import { LoginTemplate } from "../../templates";
import { signInRequest } from "../../../redux/actions/auth";
import { Button } from "antd";
import {Link} from "react-router-dom"
import { signOutRequest } from "../../../redux/actions/auth";

const DashBoardPage = () => {
  const dispatch = useDispatch();

  return <div>
       <Link to="/dashboard/search">
       <Button>Search</Button>
       </Link>
      <Button onClick={() => dispatch(signOutRequest())}>Logout</Button>
  </div>
};

export default DashBoardPage;
