import React from "react";
import { useDispatch } from "react-redux";
import { LoginTemplate } from "../../templates";
import { signInRequest } from "../../../redux/actions/auth";

const LoginPage = () => {
  const dispatch = useDispatch();

  return <LoginTemplate signInUser={(data) => dispatch(signInRequest(data))} />;
};

export default LoginPage;
