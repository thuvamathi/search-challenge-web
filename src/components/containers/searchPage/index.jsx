import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFilters, searchData } from "../../../redux/actions/search";
import { signOutRequest } from "../../../redux/actions/auth";
import { SearchTemplate } from "../../templates";
import { Button } from "antd";
import profileAvatar from "../../../assets/images/profileAvatar.jpg";


const SearchPage = () => {
    const dispatch = useDispatch();
    const { typeList } = useSelector(({ searchData }) => searchData);
    const { data } = useSelector(({ searchData }) => searchData);
    const { user } = useSelector(({ auth }) => auth);

    return (
        <>
            <div style={{ margin: "10px" }}>
                    <img src={profileAvatar} width="40px" style={{marginLeft:"8px"}}></img>
                    <p>{`${user.name || 'unknown'}`}</p>
                <Button onClick={() => { dispatch(signOutRequest()) }}>Logout</Button>
            </div>

            <SearchTemplate
                onSubmit={(value) => dispatch(searchData(value))}
                getFilters={(Item) => { dispatch(getFilters(Item)) }}
                typeList={typeList}
                data={data}
            />
        </>
    )

};

export default SearchPage;
