import React from "react";
import PropTypes from "prop-types";
import {ListItem} from "../../atoms"
import "./styles.less";
import {Form} from "antd";

const dataLayout = {
    wrapperCol: { offset: 4, span: 10 },
};

const FormList = ({ data }) => (
    <Form>
    <Form.Item {...dataLayout}>
        {
            Object.keys(data).length === 0 && data.constructor === Object ? <div style = {{textAlign:"center"}}>NO DATA</div> : <ul className="notATable">
                {
                    Object.keys(data).map((item, value) => {
                        return (
                            <ListItem name={data[item]} value={item} />
                        )
                    })
                }
            </ul>
        }
    </Form.Item>
    </Form>
);

FormList.propTypes = {
    data: PropTypes.object,
};

FormList.defaultProps = {
    data: {},

};

export default FormList;
