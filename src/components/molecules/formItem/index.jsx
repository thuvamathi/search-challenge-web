import React from "react";
import { Col, Row } from "antd";
import PropTypes from "prop-types";

import "./style.less";

const FormItem = (props) => {
  const {
    primaryInputItem,
    secondaryInputItem,
    teritaryInputItem,
    colCount,
    fillRow,
    rowSizes,
  } = props;
  const { lg, md, sm, xs } = rowSizes;
  return (
    <Row className="form-item-row">
      {colCount === 3 &&
        primaryInputItem &&
        secondaryInputItem &&
        teritaryInputItem && (
          <>
            <Col lg={lg || 8} md={md || 8} sm={sm || 24} xs={xs || 24}>
              {primaryInputItem()}
            </Col>
            <Col lg={lg || 8} md={md || 8} sm={sm || 24} xs={xs || 24}>
              {secondaryInputItem()}
            </Col>
            <Col lg={lg || 8} md={md || 8} sm={sm || 24} xs={xs || 24}>
              {teritaryInputItem()}
            </Col>
          </>
        )}
      {colCount < 3 && fillRow && primaryInputItem && (
        <Col lg={lg || 24} md={md || 24} sm={sm || 24} xs={xs || 24}>
          {primaryInputItem()}
        </Col>
      )}
      {colCount < 3 && !fillRow && primaryInputItem && (
        <Col lg={lg || 12} md={md || 12} sm={sm || 24} xs={xs || 24}>
          {primaryInputItem()}
        </Col>
      )}
      {colCount < 3 && !fillRow && secondaryInputItem && (
        <Col lg={lg || 12} md={md || 12} sm={sm || 24} xs={xs || 24}>
          {secondaryInputItem()}
        </Col>
      )}
    </Row>
  );
};

FormItem.propTypes = {
  primaryInputItem: PropTypes.func,
  secondaryInputItem: PropTypes.func,
  teritaryInputItem: PropTypes.func,
  fillRow: PropTypes.bool,
  rowSizes: PropTypes.shape({
    lg: PropTypes.number,
    md: PropTypes.number,
    sm: PropTypes.number,
    xs: PropTypes.number,
  }),
  colCount: PropTypes.number,
};

FormItem.defaultProps = {
  primaryInputItem: () => <div />,
  secondaryInputItem: () => <div />,
  teritaryInputItem: () => <div />,
  fillRow: false,
  rowSizes: {},
  colCount: 1,
};

export default FormItem;
