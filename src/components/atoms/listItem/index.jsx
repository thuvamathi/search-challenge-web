import React from "react";
import PropTypes from "prop-types";

const ListItem = ({ name,value }) => (
    <li><label>{value}</label><div>{name}</div></li>
);

ListItem.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
};

ListItem.defaultProps = {
  name: "",
  value:""
};

export default ListItem;
