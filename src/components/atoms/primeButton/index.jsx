import React from "react";
import { withRouter } from "react-router";
import { Button } from "antd";
import PropTypes from "prop-types";
import "./style.less";

const PrimeButton = withRouter(({ history, buttonName, onClick, htmlType }) => (
  <Button
    className="gx-mb-0 prime-btn"
    type="primary"
    onClick={() => onClick(history)}
    htmlType={htmlType}
  >
    {buttonName}
  </Button>
));

PrimeButton.propTypes = {
  buttonName: PropTypes.string,
  onClick: PropTypes.func,
  htmlType: PropTypes.string,
  history: PropTypes.shape({}),
};

PrimeButton.defaultProps = {
  buttonName: "ADD",
  onClick: () => {},
  htmlType: "button",
};

export default PrimeButton;
