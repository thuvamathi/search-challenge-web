import React from "react";
import PropTypes from "prop-types";
import images from "../../../assets/images";

const CircularProgress = ({ className }) => (
  <div className={`loader ${className}`}>
    <img src={images.loader} alt="loader" />
  </div>
);

CircularProgress.propTypes = {
  className: PropTypes.string,
};

CircularProgress.defaultProps = {
  className: "",
};

export default CircularProgress;
