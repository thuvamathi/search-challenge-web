import React from "react";
import PropTypes from "prop-types";
import { SearchForm } from "../../organisms";
import {FormList} from "../../molecules";

const SearchTemplate = ({ data,onSubmit,getFilters,typeList}) => {
  return (
    <>
    <SearchForm
        onSubmit = {onSubmit}
        getFilters = {getFilters}
        typeList = {typeList}
    />
    <FormList data = {data}/>
    </>
  );
};

SearchTemplate.propTypes = {
    data: PropTypes.object,
    onSubmit: PropTypes.func,
    getFilters: PropTypes.func,
    typeList: PropTypes.object,
};

export default SearchTemplate;
