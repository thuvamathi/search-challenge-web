import React from "react";
import PropTypes from "prop-types";
import "./styles.less";
import { LoginForm } from "../../organisms";

const LoginTemplate = ({ signInUser }) => {
  return (
    <div className="login-wrap">
      <div className="middle-wrap">
        <LoginForm login={signInUser} />
      </div>
    </div>
  );
};

LoginTemplate.propTypes = {
  signInUser: PropTypes.func.isRequired,
};

export default LoginTemplate;
