import React from "react";
import PropTypes from "prop-types";
import { Result } from "antd";
import "./styles.less";

const PageNotFoundTemplate = ({ status, title, subtitle }) => {
  return (
    <div className="not-found-wrap">
      <Result status={status} title={title} subTitle={subtitle} />
    </div>
  );
};

PageNotFoundTemplate.propTypes = {
  status: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
};

PageNotFoundTemplate.defaultProps = {
  status: "404",
  title: "Page Not Found",
  subtitle: "Sorry, the page you visited does not exist.",
};

export default PageNotFoundTemplate;
