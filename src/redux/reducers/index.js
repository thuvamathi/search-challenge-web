import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage/session";
import Settings from "./settings";
import Auth from "./auth";
import Search from "./search";

import Common from "./common";


const settingsPersistConfig = {
  key: "settings",
  whitelist: ["navStyle"], // example to show how the navStyle property will be persisted
  storage,
};

const commonDataPersistConfig = {
  key: "commonData",
  whitelist: [],
  storage,
};

const searhDataPersistConfig = {
  key: "searchData",
  whitelist: [],
  storage,
};

const AuthDataPersistConfig = {
  key: "auth",
  whitelist: ["accessToken","user"],
  storage,
};


export default (history) =>
  combineReducers({
    router: connectRouter(history),
    settings: persistReducer(settingsPersistConfig, Settings),
    auth: persistReducer(AuthDataPersistConfig, Auth),
    commonData: persistReducer(commonDataPersistConfig, Common),
    searchData: persistReducer(searhDataPersistConfig, Search),

  });
