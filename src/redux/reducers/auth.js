import { AUTH_TYPES, COMMON_TYPES } from "../../utilities/constants";

export const INIT_STATE = {
  loading: false,
  accessToken: "",
  user:{}
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case AUTH_TYPES.SIGN_IN + COMMON_TYPES.REQUEST: {
      return { ...state, loading: true };
    }
    case AUTH_TYPES.SIGN_IN + COMMON_TYPES.SUCCESS: {
      return {
        ...state,
        loading: false,
        accessToken: action.payload.access_token,
        user:action.payload.user
      };
    }
    case AUTH_TYPES.SIGN_IN + COMMON_TYPES.FAILURE: {
      return { ...state, loading: false };
    }
    case AUTH_TYPES.SIGN_OUT + COMMON_TYPES.REQUEST: {
      return { ...state, loading: false, accessToken: "" };
    }
    default:
      return state;
  }
};
