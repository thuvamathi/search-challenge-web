import { SEARCH_TYPES, COMMON_TYPES } from "../../utilities/constants";

export const INIT_STATE = {
  loading: false,
  typeList:{},
  data:{}
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SEARCH_TYPES.GET_FILTERS + COMMON_TYPES.REQUEST: {
      return { ...state, loading: true };
    }

    case SEARCH_TYPES.GET_FILTERS + COMMON_TYPES.SUCCESS: {
      return {
        ...state,
        loading: false,
        typeList : action.payload.response,
      };
    }
    case SEARCH_TYPES.GET_FILTERS + COMMON_TYPES.FAILURE: {
      return { ...state, loading: false };
    }

    case SEARCH_TYPES.SEARCH + COMMON_TYPES.REQUEST: {
        return { ...state, loading: true };
      }
  
      case SEARCH_TYPES.SEARCH + COMMON_TYPES.SUCCESS: {
        return {
          ...state,
          loading: false,
          data : action.payload.response,
        };
      }
      case SEARCH_TYPES.SEARCH + COMMON_TYPES.FAILURE: {
        return { ...state, loading: false };
      }
    default:
      return state;
  }
};
