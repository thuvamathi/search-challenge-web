
import { SEARCH_TYPES, COMMON_TYPES } from "../../utilities/constants";

export const getFilters = (payload) => ({
  type: SEARCH_TYPES.GET_FILTERS + COMMON_TYPES.REQUEST,
  payload,
});

export const searchData = (payload,callback) => ({
  type: SEARCH_TYPES.SEARCH + COMMON_TYPES.REQUEST,
  payload,
  callback
});
