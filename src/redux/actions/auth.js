import { AUTH_TYPES, COMMON_TYPES } from "../../utilities/constants";

export const signInRequest = (payload) => ({
  type: AUTH_TYPES.SIGN_IN + COMMON_TYPES.REQUEST,
  payload,
});

export const signOutRequest = (payload) => ({
  type: AUTH_TYPES.SIGN_OUT + COMMON_TYPES.REQUEST,
  payload,
});
