/*
 * Single Retailer Application 2.8.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import { put, takeLatest, take } from "redux-saga/effects";
import queryString from "query-string";
import {
  SEARCH_TYPES,
  COMMON_TYPES,
  MAKE_API_REQUEST,
  MESSAGES,
} from "../../utilities/constants";
import {
  getFiltersAPI,
  searchDataAPI
} from "../../utilities/apis";

export function* getFilterList(action) {
  const req = {
    type: action.payload
  };
  const payload = {
    api: getFiltersAPI,
    action: SEARCH_TYPES.GET_FILTERS,
    payload: queryString.stringify(req),
  };
  yield put({ type: MAKE_API_REQUEST, payload });
}

export function* searchData(action) {
  const payload = {
    api: searchDataAPI,
    action: SEARCH_TYPES.SEARCH,
    payload: {
        type: action.payload.type,
        column: action.payload.column,
        value: action.payload.value || ""
    },
    message: MESSAGES.SUCCESS,
  };
  yield put({ type: MAKE_API_REQUEST, payload });
  yield take(SEARCH_TYPES.SEARCH + COMMON_TYPES.SUCCESS);
  if (action.callback) {
    action.callback();
  }
}

export default function* roleSaga() {
  yield takeLatest(
    SEARCH_TYPES.GET_FILTERS + COMMON_TYPES.REQUEST,
    getFilterList
  );

  yield takeLatest(
    SEARCH_TYPES.SEARCH + COMMON_TYPES.REQUEST,
    searchData
  );
}
