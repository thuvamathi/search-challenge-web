import { put, select, take, takeLatest } from "redux-saga/effects";
import { REHYDRATE } from "redux-persist";
import Cookies from "cookies-js";
import { push } from "connected-react-router";
import {
  AUTH_TYPES,
  COMMON_TYPES,
  MAKE_API_REQUEST,
  ACCESS_TOKEN,
} from "../../utilities/constants";
import {
  signInUserAPI,
} from "../../utilities/apis";
import { setHeadersForAllInstances } from "../../utilities/apis/instances";
import { encrypt, authSelector } from "../../utilities/helpers";

export function* signInRequest(action) {
  // const encryptedPassword = encrypt(action.payload.password);

  const payload = {
    api: signInUserAPI,
    action: AUTH_TYPES.SIGN_IN,
    payload: {
      email: action.payload.username,
      password: action.payload.password,
    },
  };
  yield put({ type: MAKE_API_REQUEST, payload });

  const loginResponse = yield take(AUTH_TYPES.SIGN_IN + COMMON_TYPES.SUCCESS);
  if (
    loginResponse.payload &&
    loginResponse.payload.access_token
  ) {
    setHeadersForAllInstances(loginResponse.payload.access_token);
    if (action.payload.remember) {
      Cookies.set(ACCESS_TOKEN, loginResponse.payload.access_token, {
        expires: new Date(loginResponse.payload.data.token.expiresAt * 1000),
      });
    } else {
      Cookies.expire(ACCESS_TOKEN);
    }
  }

  // const { initialPage } = yield select(authSelector);

  // if (initialPage) {
  //   yield put(push(initialPage));
  // }
}

export function signOutRequest() {
  Cookies.expire(ACCESS_TOKEN);
}

export function* rehydrateAuth(action) {
  if (action.key === "auth") {
    let token = "";
    if (action.payload && action.payload.accessToken) {
      //  when rehydrating
      token = action.payload.accessToken;
    } else {
      token = Cookies.get(ACCESS_TOKEN);
      // menu = Cookies.get(USER_MENU);
    }
    if (token) {
      setHeadersForAllInstances(token);
    }
  }
}

export default function* authSaga() {
  yield takeLatest(AUTH_TYPES.SIGN_IN + COMMON_TYPES.REQUEST, signInRequest);
  yield takeLatest(AUTH_TYPES.SIGN_OUT + COMMON_TYPES.REQUEST, signOutRequest);
  yield takeLatest(REHYDRATE, rehydrateAuth);
}
