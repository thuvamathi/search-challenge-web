import { message } from "antd";
import { call, put, takeEvery } from "redux-saga/effects";
import * as log from "loglevel";
import {
  MAKE_API_REQUEST,
  NETWORK_STATUS_CODE,
  COMMON_TYPES,
  MESSAGES,
  AUTH_TYPES,
} from "../../utilities/constants";
import { removeHeadersForAllInstances } from "../../utilities/apis/instances";

/** Saga to be called to make API calls. Will handle all exceptions and show relevant error messages. */
function* networkRequestSaga(action) {
  const showLoading = !(
    typeof action.payload.showLoading === "boolean" &&
    !action.payload.showLoading
  );
  if (showLoading) {
    message.loading({
      content: MESSAGES.LOADING,
      duration: 0,
      key: action.payload.action,
    });
  }

  try {
    const response = yield call(
      action.payload.api,
      action.payload.payload,
      action.payload.axiosConfig
    );

    let toastMessage = MESSAGES.FAILED;

    switch (response.status) {
      case NETWORK_STATUS_CODE.SUCCESS:
        // Will fire a success action if stats code is 200
        if (action.payload.responseYupSchema) {
          action.payload.responseYupSchema.validateSync(response.data);
        }
        yield put({
          type: action.payload.action + COMMON_TYPES.SUCCESS,
          payload: response.data,
        });
        if (action.payload.message) {
          message.success({
            content: action.payload.message,
            key: action.payload.action,
          });
        } else if (showLoading) {
          message.loading({
            content: MESSAGES.LOADING,
            duration: 0.1,
            key: action.payload.action,
          });
        }

        break;
      case NETWORK_STATUS_CODE.UNAUTHORIZED:
        toastMessage = MESSAGES.UNAUTHORIZED;

        /// /////////////////////////////////
        // ADD LOGIC TO HANDLE 401 & REFRESH TOKEN
        /// /////////////////////////////////

        yield put({
          type: AUTH_TYPES.SIGN_OUT + COMMON_TYPES.REQUEST,
          payload: response.data,
        });
        removeHeadersForAllInstances();

      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.FORBIDDEN:
        if (toastMessage === MESSAGES.FAILED) {
          toastMessage = MESSAGES.INVALID_CREDENTIALS;
        }
      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.NOT_FOUND:
        if (toastMessage === MESSAGES.FAILED) {
          toastMessage = MESSAGES.NOT_FOUND;
        }
      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.INTERNAL_STATUS_ERROR:
        if (toastMessage === MESSAGES.FAILED) {
          if (response.data.error && response.data.error.length > 0) {
            toastMessage = response.data.error
              .map((error) => error.message)
              .join(", ");
          } else {
            toastMessage = MESSAGES.INTERNAL_STATUS_ERROR;
          }
        }

      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.UNPROCURABLE_ENTITY:
        console.log("response",response.data)
        if (toastMessage === MESSAGES.FAILED && response.data.error) {
          toastMessage = Object.keys(response.data.error)
            .map((key) => response.data.error[key].join(", "))
            .join(", ");
            // console.log("toastMessage",toastMessage)
        }
      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.NOT_ACCEPTABLE:
        if (toastMessage === MESSAGES.FAILED) {
          if (response.data.error) {
            toastMessage = Object.keys(response.data.error)
              .map((key) => response.data.error[key].join(", "))
              .join(", ");
          } else if (typeof response.data.status === "string") {
            toastMessage = response.data.status;
          }
        }
      // eslint-disable-next-line no-fallthrough
      case NETWORK_STATUS_CODE.EXPECTATION_FAILED:
        if (
          toastMessage === MESSAGES.FAILED &&
          typeof response.data.status === "string"
        ) {
          toastMessage = response.data.status;
        }
      // eslint-disable-next-line no-fallthrough
      default:
        message.error({
          content: toastMessage || MESSAGES.FAILED,
          key: action.payload.action,
        });
        yield put({ type: action.payload.action + COMMON_TYPES.FAILURE });
    }
  } catch (err) {
    // If an exception is made withing the requestSage, will show an error saying 'Oops, something went wrong!'
    log.error(err);
    message.error({
      content: MESSAGES.FAILED,
      key: action.payload.action,
    });
    yield put({ type: action.payload.action + COMMON_TYPES.FAILURE });
  }
}

export default function* requestSaga() {
  yield takeEvery(MAKE_API_REQUEST, networkRequestSaga);
}
