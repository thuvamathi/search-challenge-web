import {
SearchPage,
DashBoardPage
} from "../components";

import asyncComponent from "../utilities/asyncComponent";

import { PATHS } from "../utilities/constants";


export default {

  SearchPage: {
    path: PATHS.DASHBOARD.SEARCH,
    component: asyncComponent(SearchPage)
  },
  DashBoardPage: {
    path: PATHS.DASHBOARD.WILD_CARD,
    component: asyncComponent(DashBoardPage)
  },
};
