/* eslint-disable react/jsx-props-no-spreading */
/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import React from "react";
import PropTypes from "prop-types";
import { Route, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { PATHS } from "../utilities/constants";

const PublicRoute = React.memo(({ Component, ...rest }) => {
  const accessToken = useSelector(({ auth }) => auth.accessToken);
  return (
    <Route
      {...rest}
      render={(props) => {
        return !accessToken ? (
          <Component {...props} />
        ) : (
          <Redirect to={PATHS.DASHBOARD.SEARCH} />
        );
      }}
    />
  );
});

PublicRoute.propTypes = {
  Component: PropTypes.element.isRequired,
};

export default PublicRoute;
