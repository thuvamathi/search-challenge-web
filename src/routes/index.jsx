/*
 * Single Retailer Application 24.7.2020
 * Copyright © 2020 NCell. All rights reserved.
 */

import React from "react";
import { Route, Switch } from "react-router-dom";
import { useIdleTimer } from "react-idle-timer";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";
import { PATHS } from "../utilities/constants";
import PageNotFound from "../components/templates/404Template";

import screens from "./screens";
import { validatePathWithPermissions } from "../utilities/helpers";
import { signOutRequest } from "../redux/actions/auth";

const App = ({ permissions }) => {
  const dispatch = useDispatch();
  useIdleTimer({
    timeout: 900000, // 15 min
    onIdle: () => dispatch(signOutRequest()),
  });
  return (
    <div className="gx-main-content-wrapper">
      <Switch>
        {Object.keys(screens).map(
          (key) => (
            <Route
              key={key}
              path={screens[key].path}
              component={screens[key].component}
              exact
            />
          )
        )}
        <Route path={PATHS.WILD_CARD} component={PageNotFound} />
      </Switch>
    </div>
  );
};

App.propTypes = {
  permissions: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default App;
