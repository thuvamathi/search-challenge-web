module.exports = {
  transform: {
    "^.+\\.jsx?$": "babel-jest",
    "^.+\\.js?$": "babel-jest",
  },
  moduleNameMapper: {
    ".+\\.(css|styl|less|sass|scss|png|jpg|gif|ttf|woff|woff2)$":
      "identity-obj-proxy",
  },
};
